## Portfolio 

This is a test portfolio created at bootcamp program at Jyaasa. 

---

## Technologies Used
* HTML
* CSS
* JS
* FONTS from Google

---

## Header
- Logo 
- Navbar

## Body 
- Buttons
- About me
- Send Me Message 

## Footer 
- Contact Me
- Social Media Buttons 

---

### CSS Components used to design 
```
CSS components such as hover, transition, animation are used.
```

### JS components 
```
Validation 
Scrolling 
Carausel 
```
---
_NOTE: This site is totally beta loads of things are to be added in the mean time_